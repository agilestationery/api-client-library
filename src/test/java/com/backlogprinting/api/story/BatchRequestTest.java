package com.backlogprinting.api.story;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class BatchRequestTest {

	public static final String EMAIL = "example@example.org";
	private ObjectMapper mapper;
	private ObjectWriter objectWriter;

	@Before
	public void setUp() throws Exception {

		mapper = new ObjectMapper();
		objectWriter = mapper.writerFor(BatchRequest.class);

	}


	@Test
	public void shouldSerialiseEmailField() throws JsonProcessingException {

		BatchRequest request = new BatchRequest();
		request.setEmail(EMAIL);

		String json = objectWriter.writeValueAsString(request);

		assertThat(json, containsString(EMAIL));
		assertThat(json, containsString("email"));

	}

}