package com.backlogprinting.api.story;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class OrderRequestTest {

	private ObjectWriter writer;
	private ObjectMapper mapper;

	@Before
	public void createWriter() {
		mapper = new ObjectMapper();
		writer = mapper.writerFor(OrderRequest.class);
	}

	@Test
	public void shouldSerialiseToExpectedJson() throws IOException {
		OrderRequest request = new OrderRequest("test");
		String jsonForm = writer.writeValueAsString(request);
		JsonNode result = mapper.readTree(jsonForm);

		assertTrue(result.get("batchId").isTextual());
		assertThat(result.get("batchId").asText(),is("test"));
	}

}