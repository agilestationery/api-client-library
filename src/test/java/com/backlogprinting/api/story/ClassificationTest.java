package com.backlogprinting.api.story;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.Before;
import org.junit.Test;
import uk.co.agilestationery.colour.StoryColour;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ClassificationTest {


	private ObjectWriter writer;
	private ObjectMapper mapper;

	@Before
	public void createWriter() {
		mapper = new ObjectMapper();
		writer = mapper.writerFor(Classification.class);
	}


	@Test
	public void shouldSerialiseToFlatPair() throws IOException {
		Classification classification = new Classification("example", StoryColour.RED.getColour());
		String jsonForm = writer.writeValueAsString(classification);
		JsonNode result = mapper.readTree(jsonForm);

		assertTrue(result.get("colour").isTextual());
		assertThat(result.get("label").asText(),is("example"));
		assertThat(result.get("colour").asText(),equalToIgnoringCase("#ED1C24"));


	}

}