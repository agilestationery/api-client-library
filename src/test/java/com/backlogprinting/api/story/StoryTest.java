package com.backlogprinting.api.story;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import org.junit.Before;
import org.junit.Test;
import uk.co.agilestationery.colour.HexColour;

import java.io.IOException;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class StoryTest
{

	public static final HexColour RED = new HexColour("#ff0000");
	private static final HexColour GREEN = new HexColour("#00ff00");

	private ObjectMapper mapper;
	private ObjectReader reader;

	@Before
	public void createReader() {
		mapper = new ObjectMapper();
		reader = mapper.readerFor(Story.class);
	}

	@Test
	public void shouldAcceptLabels() throws IOException {
		String jsonWithLabels = "{\n" +
				"\t\"labels\" : [\n" +
				"\t\t{\n" +
				"\t\t\t\"label\" : \"bug\",\n" +
				"\t\t\t\"colour\" : \"#ff0000\"\n" +
				"\t\t},\n" +
				"\t\t{\n" +
				"\t\t\t\"label\" : \"easy\",\n" +
				"\t\t\t\"colour\" : \"#00ff00\"\n" +
				"\t\t}\n" +
				"\t]\n" +
				"}";

		Story story = reader.readValue(jsonWithLabels);

		assertEasyBug(story.getLabels());

	}

	@Test
	public void shouldAcceptClassifications() throws IOException {
		String jsonWithClassifications = "{\n" +
				"\t\"classifications\" : [\n" +
				"\t\t{\n" +
				"\t\t\t\"label\" : \"bug\",\n" +
				"\t\t\t\"colour\" : \"#ff0000\"\n" +
				"\t\t},\n" +
				"\t\t{\n" +
				"\t\t\t\"label\" : \"easy\",\n" +
				"\t\t\t\"colour\" : \"#00ff00\"\n" +
				"\t\t}\n" +
				"\t]\n" +
				"}";

		Story story = reader.readValue(jsonWithClassifications);

		assertEasyBug(story.getClassifications());
	}

	private void assertEasyBug(List<Classification> classificationList) {
		assertEquals(2, classificationList.size());

		assertEquals("bug",classificationList.get(0).getLabel());
		assertEquals(RED,classificationList.get(0).getColour());

		assertEquals("easy",classificationList.get(1).getLabel());
		assertEquals(GREEN,classificationList.get(1).getColour());
	}

	@Test
	public void acceptingLabelsShouldNotAddClassifications() throws IOException {
		String jsonWithLabels = "{\n" +
				"\t\"labels\" : [\n" +
				"\t\t{\n" +
				"\t\t\t\"label\" : \"bug\",\n" +
				"\t\t\t\"colour\" : \"#ff0000\"\n" +
				"\t\t},\n" +
				"\t\t{\n" +
				"\t\t\t\"label\" : \"easy\",\n" +
				"\t\t\t\"colour\" : \"#00ff00\"\n" +
				"\t\t}\n" +
				"\t]\n" +
				"}";

		Story story = reader.readValue(jsonWithLabels);

		assertEquals(0,story.getClassifications().size());

	}

	@Test
	public void addingLabelsShouldAddClassification()
	{
		Story story = new Story();
		story.addLabel(new Classification("bug", RED));

		assertEquals(1,story.getClassifications().size());
		assertEquals(1,story.getLabels().size());
	}

}
