package com.backlogprinting.api.story;

/**
 * @author Simon
 * @since 28/11/2017
 */
public class OrderRequest {

	private String batchId;

	public OrderRequest(String batchId) {
		this.batchId = batchId;
	}

	public String getBatchId() {
		return batchId;
	}
}
