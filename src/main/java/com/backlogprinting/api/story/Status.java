package com.backlogprinting.api.story;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Status {

	@JsonProperty
	private String label;

	@JsonProperty
	private String category;

	public Status() {
	}

	public Status(String label, String category) {
		this.label = label;
		this.category = category;
	}

	public String getLabel() {
		return label;
	}

	public String getCategory() {
		return category;
	}
}
