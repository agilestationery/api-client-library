package com.backlogprinting.api.story;

import javax.validation.constraints.Pattern;

/**
 * BatchId
 *
 * @author Simon
 */
@Pattern(regexp = "[A-Za-z0-9]+")
public @interface BatchId {
}
