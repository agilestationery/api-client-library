package com.backlogprinting.api.story;

/**
 * A set of shipping and contact details related to a batch.
 * Distinct from the notion of users, which are longer lived mutable
 * entities.
 * Mutability should end for Customers when the order is placed.
 * @author Simon
 * @since 02/08/2017
 */
public class Shipping {
	private String email;
	private String phone;
	private String firstName;
	private String lastName;
	private String address;
	private String apartment;
	private String city;
	private String country;
	private String postcode;
	private boolean newsletter;


	public static Shipping fromBatch(Batch batch) {
		Shipping result = new Shipping();
		result.setEmail(batch.getEmail());
		return result;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getApartment() {
		return apartment;
	}

	public void setApartment(String apartment) {
		this.apartment = apartment;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public boolean isNewsletter() {
		return newsletter;
	}

	public void setNewsletter(boolean newsletter) {
		this.newsletter = newsletter;
	}
}
