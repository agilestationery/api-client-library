package com.backlogprinting.api.story;

import com.backlogprinting.api.Patterns;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * BatchRequest
 *
 * @author Simon
 */
public class BatchRequest {

	public BatchRequest() {
	}

	@Future
	private LocalDate workshopDate;

  	@NotNull @Size(min=5)
	@Pattern(regexp= Patterns.EMAIL_PATTERN)
	private String email;

	private boolean importing;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setWorkshopDate(LocalDate workshopDate) {
		this.workshopDate = workshopDate;
	}

	public LocalDate getWorkshopDate() {
		return workshopDate;
	}

	public boolean isImporting() {
		return importing;
	}

	public void setImporting(boolean importing) {
		this.importing = importing;
	}

}
