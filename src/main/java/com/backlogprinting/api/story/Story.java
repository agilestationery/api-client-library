package com.backlogprinting.api.story;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.unmodifiableList;

/**
 * Story
 *
 * @author Simon
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Story {

	private String title;

	@Pattern(regexp = "[A-Za-z0-9.-]}")
	private String ref;

	private String text;

	@Size(max = 10)
	private String points;

	private List<Classification> classifications; // old name - deprecated
	private List<Classification> labels; // new name
	private String author;
	private OffsetDateTime createdDate;
	private String project;
	private String fixVersion;
	private Classification type;
	private Status status;
	private String originApplication;

	public Story() {
		this.classifications = new ArrayList<>(3);
		this.labels = new ArrayList<>(3);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getPoints() {
		return points;
	}

	public void setPoints(String points) {
		this.points = points;
	}

	public void addLabel(Classification classification) {
		classifications.add(classification);
		labels.add(classification);
	}

	public List<Classification> getClassifications() {
		return unmodifiableList(classifications);
	}

	public List<Classification> getLabels() {
		return unmodifiableList(labels);
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAuthor() {
		return author;
	}

	public void setCreatedDate(OffsetDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public OffsetDateTime getCreatedDate() {
		return createdDate;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getProject() {
		return project;
	}

	public String getFixVersion() {
		return fixVersion;
	}

	public void setFixVersion(String fixVersion) {
		this.fixVersion = fixVersion;
	}

	public Classification getType() {
		return type;
	}

	public void setType(Classification type) {
		this.type = type;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Status getStatus() {
		return status;
	}

	public String getOriginApplication() {
		return originApplication;
	}

	public void setOriginApplication(String originApplication) {
		this.originApplication = originApplication;
	}
}
