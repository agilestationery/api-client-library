package com.backlogprinting.api.story;

/**
 * @author Simon
 * @since 28/11/2017
 */
public class Order {
	private long id;
	private String batchId;
	private String owner;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}
}
