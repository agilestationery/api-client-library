package com.backlogprinting.api.story;

import java.time.LocalDateTime;

/**
 * @author Simon
 * @since 23/11/2017
 */
public class BatchPatchRequest {

	private LocalDateTime orderedDate;
	private Boolean importing;

	public LocalDateTime getOrderedDate() {
		return orderedDate;
	}

	public void setOrderedDate(LocalDateTime orderedDate) {
		this.orderedDate = orderedDate;
	}

	public Boolean getImporting() {
		return importing;
	}

	public void setImporting(Boolean importing) {
		this.importing = importing;
	}
}
