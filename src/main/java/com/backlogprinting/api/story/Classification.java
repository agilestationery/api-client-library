package com.backlogprinting.api.story;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import uk.co.agilestationery.colour.HexColour;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Simon
 * @since 19/07/2017
 */
public class Classification {

	private String label;
	private HexColour colour;

	public Classification(
			@JsonProperty("label")
			String label,
			@JsonProperty("colour")
			HexColour colour) {
		this.label = label;
		this.colour = colour;
	}

	public String getLabel() {
		return label;
	}

	public HexColour getColour() {
		return colour;
	}

	@JsonValue
	public Map<String,String> jsonMap() {
		Map<String, String> map = new LinkedHashMap<>();
		map.put("label", label);
		map.put("colour",colour.getHex());
		return map;
	}
}
