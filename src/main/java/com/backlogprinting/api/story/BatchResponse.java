package com.backlogprinting.api.story;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Simon
 * @since 22/11/2017
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class BatchResponse {

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
