package com.backlogprinting.api.story;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * Batch
 *
 * @author Simon
 */
public class Batch {

	private String email;

	private List<BatchEntry> stories;

	@DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
	private LocalDate workshopDate;

	private boolean importing;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private ZonedDateTime orderedDate;

	public Batch() {
		stories = new ArrayList<>(7);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<BatchEntry> getStories() {
		return stories;
	}

	public LocalDate getWorkshopDate() {
		return workshopDate;
	}

	public void setWorkshopDate(LocalDate workshopDate) {
		this.workshopDate = workshopDate;
	}

	public void setImporting(boolean importing) {
		this.importing = importing;
	}

	public boolean isImporting() {
		return importing;
	}

	public void setOrderedDate(ZonedDateTime orderedDate) {
		this.orderedDate = orderedDate;
	}

	public ZonedDateTime getOrderedDate() {
		return orderedDate;
	}

}
