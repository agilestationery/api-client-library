package com.backlogprinting.api.story;

import javax.ws.rs.core.GenericType;
import java.util.List;

/**
 * Holds type descriptions for typed collections.
 * @author Simon
 * @since 23/11/2017
 */
public class CollectionTypes {

	public static GenericType<List<Notification>> notificationListType()  {
		return new GenericType<List<Notification>>() {};
	}

}
