package com.backlogprinting.api.story;

/**
 * Notification
 *
 * @author Simon
 */
public class Notification {

	private String message;

	public Notification(String message) {

		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
