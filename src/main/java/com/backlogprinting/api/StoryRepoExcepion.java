package com.backlogprinting.api;

public class StoryRepoExcepion extends RuntimeException {
	public StoryRepoExcepion(Throwable cause) {
		super(cause);
	}

	public StoryRepoExcepion(String message) {
		super(message);
	}
}
