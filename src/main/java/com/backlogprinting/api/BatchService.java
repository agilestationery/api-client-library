package com.backlogprinting.api;

import com.backlogprinting.api.story.Batch;
import com.backlogprinting.api.story.BatchPatchRequest;
import com.backlogprinting.api.story.BatchRequest;
import com.backlogprinting.api.story.BatchResponse;
import com.backlogprinting.api.story.Notification;
import com.backlogprinting.api.story.Shipping;
import com.backlogprinting.api.story.Story;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.backlogprinting.api.login.SecurityConstants;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.net.URI;


import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import static javax.ws.rs.client.Entity.json;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static javax.ws.rs.core.UriBuilder.fromUri;


/**
 * BatchService
 *
 * @author Simon
 */
public class BatchService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BatchService.class);
	private final Client client;
	private final URI batchesCollection;

	public BatchService(Client client, StoryRepositoryConfiguration repoConfig) {
		this.client = client;

		batchesCollection = fromUri(repoConfig.getBaseUri()).path("/batches").build();

	}

	public String create(BatchRequest batchRequest, String accessToken) {

		checkTokenAndThrow(accessToken);

		Entity<BatchRequest> entity = json(batchRequest);

		try {
			BatchResponse response = client.target(batchesCollection).request()
					.accept(APPLICATION_JSON_TYPE)
					.header(SecurityConstants.HEADER_NAME, SecurityConstants.TOKEN_PREFIX + accessToken)
					.post(entity, BatchResponse.class);

			return response.getId();
		} catch (NotFoundException nfe) {
			LOGGER.error("Not found: {}", batchesCollection);
			throw new StoryRepoExcepion(nfe);
		}
	}

	public Batch fetch(String id, String accessToken) {

		checkTokenAndThrow(accessToken);

		URI batchUri = buildBatchUri(id);

		return client.target(batchUri).request()
				.accept(APPLICATION_JSON_TYPE)
				.header(SecurityConstants.HEADER_NAME, SecurityConstants.TOKEN_PREFIX + accessToken)
				.get(Batch.class);
	}

	private void checkTokenAndThrow(String accessToken) {
		checkNotNull(accessToken, "token mull");
		checkArgument(!accessToken.isEmpty(), "token empty");
	}

	public URI addStory(String id, Story story, String accessToken) {

		checkNotNull(id);
		checkNotNull(story);
		checkTokenAndThrow(accessToken);

		URI batchUri = buildBatchUri(id);

		Entity<Story> entity = json(story);

		Response response = null;
		try {
			response = client.target(batchUri).request()
					.accept(APPLICATION_JSON_TYPE)
					.header(SecurityConstants.HEADER_NAME, SecurityConstants.TOKEN_PREFIX + accessToken)
					.post(entity);

			throwOnFailures(response);

			return response.getLocation();
		} finally {
			if(response!=null) {
				response.close();
			}
		}

	}

	private void throwOnFailures(Response response) {
		if(response.getStatus()>299) {
			String errorBody = response.readEntity(String.class);
			throw new StoryRepoExcepion(errorBody);
		}
	}

	private URI buildBatchUri(String id) {
		return fromUri(batchesCollection)
				.path("/{batchId}")
				.build(id);
	}

	public void addNotification(String batchId, String accessToken, String message) {

		checkTokenAndThrow(accessToken);

		URI notificationsCollection = buildNotificationsCollectionUri(batchId);

		Notification notification = new Notification(message);

		Response response = client.target(notificationsCollection).request()
				.header(SecurityConstants.HEADER_NAME, SecurityConstants.TOKEN_PREFIX + accessToken)
				.post(json(notification));

		throwOnFailures(response);
	}

	private URI buildNotificationsCollectionUri(String batchId) {
		return fromUri(buildBatchUri(batchId)).path("/notifications").build();
	}

	private URI buildShippingUri(String batchId) {
		return fromUri(buildBatchUri(batchId)).path("/shipping").build();
	}

	public void finishImports(String batchId, String accessToken) {

		checkTokenAndThrow(accessToken);

		BatchPatchRequest patchRequest = new BatchPatchRequest();
		patchRequest.setImporting(false);
		Response response = client.target(buildBatchUri(batchId)).request()
				.header(SecurityConstants.HEADER_NAME, SecurityConstants.TOKEN_PREFIX + accessToken)
				.method("PATCH", json(patchRequest));

		throwOnFailures(response);
	}

	public boolean isImporting(String batchId, String accessToken) {
		return fetch(batchId, accessToken).isImporting();
	}

	public Shipping fetchCustomer(String batchId, String accessToken) {

		checkTokenAndThrow(accessToken);

		return client.target(buildShippingUri(batchId)).request()
				.header(SecurityConstants.HEADER_NAME, SecurityConstants.TOKEN_PREFIX + accessToken)
				.get(Shipping.class);
	}

	public Shipping updateCustomerShipping(String batchId, String accessToken, Shipping shipping) {

		checkTokenAndThrow(accessToken);

		return client.target(buildShippingUri(batchId)).request()
				.header(SecurityConstants.HEADER_NAME, SecurityConstants.TOKEN_PREFIX + accessToken)
				.put(json(shipping), Shipping.class);
	}


}
