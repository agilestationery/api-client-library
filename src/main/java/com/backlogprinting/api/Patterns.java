package com.backlogprinting.api;

/**
 * @author Simon
 * @since 15/09/2017
 */
public class Patterns {
	public static final String EMAIL_PATTERN = ".*?@.+?\\..+?";
	public static final String PHONE_PATTERN = "\\+?[0-9(][0-9)(# -]+$";
}
