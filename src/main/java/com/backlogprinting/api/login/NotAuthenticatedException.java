package com.backlogprinting.api.login;


public class NotAuthenticatedException extends LoginFailedException {

	public NotAuthenticatedException(String message) {
		super(message);
	}
}
