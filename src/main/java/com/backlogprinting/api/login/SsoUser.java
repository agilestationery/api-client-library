package com.backlogprinting.api.login;

/**
 * Wraps the inverse functional properties necessary to identify the user.
 *
 * Client version.
 *
 * @author Simon
 * @since 10/10/2017
 */
public class SsoUser {

	private String email;

	public SsoUser(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

}
