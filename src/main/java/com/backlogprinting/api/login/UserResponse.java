package com.backlogprinting.api.login;

import java.net.URI;

/**
 * Wraps a URL such that the location of a user profile will be known to parties
 * invoking manipulations on a user entity (create, login etc).
 * @author Simon
 * @since 10/10/2017
 */
public class UserResponse {

	private URI url;

	public UserResponse(URI url) {
		this.url = url;
	}

	public URI getUrl() {
		return url;
	}


}
