package com.backlogprinting.api.login;

/**
 * @author Simon
 * @since 09/10/2017
 */
public interface SecurityConstants {

	String TOKEN_PREFIX = "Bearer ";
	String HEADER_NAME = "Authorization";

	/**
	 * Well-known parties
	 */
	String STORY_REPOSITORY = "story-repository";


}
