package com.backlogprinting.api;


import java.net.URI;

/**
 * GitHubCredentialsConfiguration
 *
 * @author Simon
 */

public class StoryRepositoryConfiguration {

	private URI baseUri;

	public URI getBaseUri() {
		return baseUri;
	}

	public void setBaseUri(URI baseUri) {
		this.baseUri = baseUri;
	}


}
