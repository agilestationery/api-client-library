# api-client

Client types and basic CRUD for batches and stories.

## Status

Split from a larger internal code-base this library may 
be incomplete and lacks tests.

Pull requests are considered helpful.

simon@agilestationery.oc.uk

## Maven

First add the Agile Stationery public artifacts repo:

	<repositories>
		<repository>
			<id>cloudrepo-agilestationery-public</id>
			<url>https://cantorva.mycloudrepo.io/public/repositories/agilestationery-public</url>
		</repository>
	</repositories>
 
 
 Then add the dependency:
 
	<dependency>
		<groupId>com.backlogprinting</groupId>
		<artifactId>api-client-library</artifactId>
		<version>0.0.8</version>
	</dependency>